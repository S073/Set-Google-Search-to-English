# Set-Google-Search-to-English

A userscript that automatically changes the google search language to English.

## Install
Install [Violentmonkey](https://addons.mozilla.org/en-US/firefox/addon/violentmonkey/), [Tampermonkey](https://addons.mozilla.org/en-US/firefox/addon/tampermonkey/), or [Greasemonkey](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/), then **[click here to install the userscript](https://codeberg.org/S073/Set-Google-Search-to-English/raw/branch/main/set-google-search-to-english.user.js)**.
