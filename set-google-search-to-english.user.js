 // ==UserScript==
// @name          Set google search to english automatically
// @description   This userscript automatically changes google search's language to english
// @namespace     https://codeberg.org/S073/Set-Google-Search-to-English
// @support       https://codeberg.org/S073/Set-Google-Search-to-English/issues
// @match         *://*.google.*/*
// @grant         none
// @version       1.3
// @author        S073
// @license       GNU GPL
// @run-at        document-start
// @created       2023-05-11
// ==/UserScript==

(function() {
    if (window.top == window.self) {
        if (window.location.href.includes("hl")) {
            if (!(window.location.href.includes("hl=en"))){
                window.location.search = window.location.search.replace(/(hl=.*&)|(hl=.*$)/gm, "hl=en&");
            }
        } else {
            window.location.search += "&hl=en";
        }
    }
})();
